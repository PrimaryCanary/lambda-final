#include <iostream>
#include <string>
#include <vector>

class Expr {
public:
  virtual std::string str() const = 0;
  virtual double eval() const = 0;
};

class Constant : public Expr {
private:
  const double _value;

public:
  Constant(double v) : _value(v) {}

  std::string str() const { return std::to_string(_value); }

  double eval() const { return _value; }
};

class BinaryAdd : public Expr {
private:
  const Expr &_left, &_right;

public:
  BinaryAdd(const Expr &l, const Expr &r) : _left(l), _right(r) {}

  std::string str() const { return _left.str() + " + " + _right.str(); }

  double eval() const { return _left.eval() + _right.eval(); }
};

class UnaryNegate : public Expr {
private:
  const Expr &_exp;

public:
  UnaryNegate(const Expr &e) : _exp(e) {}

  std::string str() const { return "-(" + _exp.str() + ")"; }

  double eval() const { return -_exp.eval(); }
};

int main() {
  const auto e(UnaryNegate(
      BinaryAdd(Constant(0.1), BinaryAdd(Constant(2.5), (Constant(4.1))))));
  std::cout << e.str() << " = " << e.eval() << std::endl;

  return 0;
}
