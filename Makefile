CXX=g++
CXXFLAGS=-Wall
CPP_FILES=$(wildcard *.cpp)
CPP_OBJECTS=$(CPP_FILES:.cpp=.o)
CPP_EXEC=cpp-expr

GHC=ghc
GHC_FLAGS=-dynamic
GHC_FILES=$(wildcard *.hs)
GHC_EXEC=hs-expr

CLJ=clojure
CLJ_FLAGS=
CLJ_FILES=$(wildcard *.clj)
CLJ_EXEC=

LATEX=latexmk
LATEX_FLAGS=-shell-escape -pdf -dvi-
LATEX_FILES=$(wildcard *.tex)

RM=rm -rf

.PHONY: clean run run/cpp run/hs run/clj

run: run/cpp run/hs run/clj
build: build/cpp build/hs build/latex

run/cpp: build/cpp
	./$(CPP_EXEC)
	@echo
build/cpp: objects
	$(CXX) $(CXXFLAGS) $(CPP_OBJECTS) -o $(CPP_EXEC)
objects: $(CPP_FILES)
	$(CXX) $(CXXFLAGS) -c $(CPP_FILES)

run/hs: build/hs
	./$(GHC_EXEC)
	@echo
build/hs: $(GHC_FILES)
	$(GHC) $(GHC_FLAGS) $(GHC_FILES) -o $(GHC_EXEC)

run/clj:
	$(CLJ) -M $(CLJ_FILES)
	@echo

build/latex:
	$(LATEX) $(LATEX_FLAGS) $(LATEX_FILES)

clean:
	latexmk -c
	$(RM) $(wildcard *.o) $(CPP_EXEC) $(wildcard *.hi) $(GHC_EXEC) $(wildcard *.xml \
	    *.aux *.bbl *.blg *-blx.bib *.gz) _minted-final/ $(LATEX_FILES:.tex=.pdf)
