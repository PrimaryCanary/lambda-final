module Main where

-- data Expr = Constant Double | BinaryAdd Expr Expr

-- eval :: Expr -> Double
-- eval (Constant c) = c
-- eval (BinaryAdd l r) = eval l + eval r

-- str :: Expr -> String
-- str (Constant c) = show c
-- str (BinaryAdd l r) = str l ++ " + " ++ str r

data Constant = Constant Double

data BinaryAdd left right = BinaryAdd left right

class Expr e

instance Expr Constant

instance (Expr left, Expr right) => Expr (BinaryAdd left right)

class (Expr e) => Eval e where
  eval :: e -> Double

instance Eval Constant where
  eval (Constant c) = c

instance (Eval left, Eval right) => Eval (BinaryAdd left right) where
  eval (BinaryAdd l r) = eval l + eval r

class (Expr e) => Str e where
  str :: e -> String

instance Str Constant where
  str (Constant c) = show c

instance (Str left, Str right) => Str (BinaryAdd left right) where
  str (BinaryAdd l r) = str l ++ " + " ++ str r

data UnaryNegate exp = UnaryNegate exp

instance (Expr exp) => Expr (UnaryNegate exp)

instance (Eval exp) => Eval (UnaryNegate exp) where
  eval (UnaryNegate e) = - (eval e)

instance (Str exp) => Str (UnaryNegate exp) where
  str (UnaryNegate e) = "-(" ++ str e ++ ")"

main = do
  let exp = BinaryAdd (Constant 2.4) (BinaryAdd (Constant 1.0) (Constant 3.0))
  print (str exp)
  print (eval exp)
  let exp = BinaryAdd (Constant 2.4) (BinaryAdd (UnaryNegate (Constant 1.0)) (Constant 3.0))
  print (str exp)
  print (eval exp)
