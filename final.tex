\documentclass[11pt]{article}

%========================
% MARGINS
%========================
\oddsidemargin=0in
\evensidemargin=0in
\textwidth=6.5in
\topmargin=-0.75in
\textheight=8.75in
\headheight=39pt
\parindent=0in
%========================
% MACROS
%========================
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{final.bib}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{underscore}
\usepackage[section]{placeins} %forces figures to appear in section
\usepackage{enumerate}
\usepackage{float}
\restylefloat{table}
\setlength\parindent{0pt} %removes paragraph indentation
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bbm}
\usepackage[section]{minted}
\usepackage{pgfplots}
\pgfplotsset{compat=1.10}
\usepackage{centernot}
\usepackage{lipsum}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\usepackage{esint}
\usepackage{booktabs}
\usepackage{verbatim}
\usepackage{bm}
\usepackage{array}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
%\input{custom_cmds} %file of my custom cmds

\newcommand{\name}{Aaron McClellan}
\newcommand{\collabs}{Jane McClellan}
\newcommand{\hwtitle}{\bfseries~\LARGE Exploring the Expression Problem}
\newcommand{\classNum}{COSC 5010 03}
\newcommand{\classTitle}{Advanced Lambda Calculus and Types}
\newcommand{\semester}{Spring 2021}

%%%%%%%%%%% HEADER/FOOTER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%HEADER
\pagestyle{fancy} {
  \fancyhead{}
  \chead{\hwtitle}
  \rhead{\name \\ \classNum}
  \cfoot{\thepage}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\fancypagestyle{firststyle}
{
   \fancyhf{}
   \rhead{\name \\ In collaboration with \collabs \\ \classNum: \classTitle \\ \semester \\ \centering \hwtitle}
   \renewcommand{\headrulewidth}{0pt}
   \setlength{\headheight}{49pt}
   % FOOTER
   \cfoot{\thepage~of~\pageref{LastPage}}
}

%%%%%%%%%%%% TITLE %%%%%%%%%%%%%%%%%%%%%%%%
\title{\vspace{-0.75in} \hwtitle}
\author{}
\date{}

\newcommand{\calclang}{\textit{CalcLang}}
\begin{document}
\thispagestyle{firststyle}

% Introduction
Alan Turing, Alonzo Church, Kurt Gödel, and many others have proven the existence and equivalence of computational models with the ability to model arbitrary computations. However these models, while powerful and useful for formal methods, see little time in the direct limelight of applied computer science. Since its inception, programming has abstracted away increasing quantities of the underlying machine and logic. First registers, then memory, then system resources; now some programming languages have relegated the implementation of their own syntax to a library~\cite{ThomasMertes-2021}. This trend seems to suggest that measures of power of formal models like the lambda calculus are too optimistic for the applied fields of programming. More refined indicators of power were needed (and perhaps still are). One such measure is the expression problem, an informal indicator formulated by Dr. Philip Wadler in 1998 to capture the expressive power of programming languages. It reads:
\begin{quote}
  The goal is to define a datatype by cases, where one can add new cases to the datatype and new functions over the datatype, without recompiling existing code, and while retaining static type safety (e.g., no casts)~\cite{wadler1998}.
\end{quote}
In this article, we will provide examples and detailed walkthroughs of various languages' solutions (and non-solutions) to the expression problem. The code and an accompanying Makefile is available at \url{https://gitlab.com/PrimaryCanary/lambda-final}.


\section*{A Motivating Example}
Consider the case of writing an interpreter for a language called \calclang{}. \calclang{} is a language for writing and evaluating mathematical expressions. Operations and dataypes must be added incrementally as \calclang{}'s interpreter becomes more complete and as the language evolves to support more mathematics. We begin with the standard model of a simplistic expressions. An expression is a tree of expressions with the leaves being constants. Of course, \calclang{} must be able to evaluate the expressions, so we must also require an expression describe how to evaluate itself. Additionally, we require an expression describe how to convert itself to a string. In C++, we might implement required behavior with an interface.
\begin{minted}{cpp}
class Expr {
public:
  virtual std::string str() const = 0;
  virtual double eval() const = 0;
};
\end{minted}
Every node in the expression tree will need to implement the interface. A constant node may implement it like this:
\begin{minted}{cpp}
class Constant : public Expr {
private:
  const double _value;

public:
  Constant(double v) : _value(v) {}
  std::string str() const { return std::to_string(_value); }
  double eval() const { return _value; }
};
\end{minted}

Standard binary addition looks very similar.
\begin{minted}{cpp}
class BinaryAdd : public Expr {
private:
  const Expr &_left, &_right;

public:
  BinaryAdd(const Expr &l, const Expr &r) : _left(l), _right(r) {}
  std::string str() const { return _left.str() + " + " + _right.str(); }
  double eval() const { return _left.eval() + _right.eval(); }
};
\end{minted}

The implementation can be tested quite easily.
\begin{minted}{cpp}
int main() {
  const auto e(
      BinaryAdd(Constant(0.1), BinaryAdd(Constant(2.5), (Constant(4.1)))));
  std::cout << e.str() << " = " << e.eval() << std::endl;

  return 0;
}
\end{minted}
\begin{verbatim}
$ ./cpp-expr
0.100000 + 2.500000 + 4.100000 = 6.7
\end{verbatim}

Now let's take a step back and examine how extensible this barebones \calclang{} implementation is. In particular, does the implementation satisfy the expression problem? Clearly we retain C++'s static type safety. Defining new datatypes (expression types) appears to be easy. The only requirement is to create a new subclass of \verb|Expr| and implement the virtual functions therein. Unary negation could be defined with such a pattern.
\begin{minted}{cpp}
class UnaryNegate : public Expr {
private:
  const Expr &_exp;

public:
  UnaryNegate(const Expr &e) : _exp(e) {}
  std::string str() const { return "-(" + _exp.str() + ")"; }
  double eval() const { return -_exp.eval(); }
};
\end{minted}

This requires no modifcation (thus compilation) of existing code and yields the correct result.
\begin{minted}{cpp}
int main() {
  const auto e(UnaryNegate(
      BinaryAdd(Constant(0.1), BinaryAdd(Constant(2.5), (Constant(4.1))))));
  std::cout << e.str() << " = " << e.eval() << std::endl;

  return 0;
}
\end{minted}
\begin{verbatim}
$ ./cpp-expr
-(0.100000 + 2.500000 + 4.100000) = -6.7
\end{verbatim}

Now consider the case of adding new operations to the expression tree. Perhaps the \calclang{} standard requires typechecking on expression trees to avoid undefined operations (e.g. addition of a real number and a set). To do so, we must add a \verb|typecheck| function to the \verb|Expr| class then implement it for every expression type so far. This necessitates modifying old code, thus failing the expression problem.
% TODO talk about actually solving it in C++ and why you should strive to solve it
\section*{The Expression Matrix}
Object-oriented languages bundle types and operations together. Functional languages tend to take a similar but orthogonal approach. Haskell in particular emphasizes the use of algebraic data types to represent types and functions that operate them to implement behavior. The expression problem becomes more enlightening when this dichotomy is recognized. We will see basic Haskell fail to satisfy the expression problem in a different manner.

Consider again the case of \calclang{}. We will implement the same expressions (sans evaluation, for now), this time in Haskell.
\begin{minted}{haskell}
data Expr = Constant Double | BinaryAdd Expr Expr
str :: Expr -> String
str (Constant c) = show c
str (BinaryAdd l r) = str l ++ " + " ++ str r
\end{minted}

Adding and implementing additional behaviors is easy. We add expression evaluation.
\begin{minted}{haskell}
eval :: Expr -> Double
eval (Constant c) = c
eval (BinaryAdd l r) = eval l + eval r
\end{minted}

Once again, we step back and examine the extensibility of this interpreter architecture. It is easy to add operations but we run into the reverse problem of C++; now it is impossible to add datatypes without modifying existing code. In order to add negation, an additional pattern must be added to the \verb|str| function. The expression problem has arisen again, albeit from a different perspective.

With these experiences, it is possible to somewhat more formally model the expression problem. Let a matrix represent the collection of datatypes and operations, where each row corresponds to a dataype to support and each column corresponds to an operation to be implemented. An element of the matrix may be zero or one, which represents the inability and ability to be implemented in a manner satisyfing the expression problem, respectively.

The C++ program's matrix can be found in table~\ref{tab:cpp} while the presented Haskell program's matix can be found in table~\ref{tab:haskell}.
\begin{table}
  \centering
  \begin{tabular}{rccc}
    & \verb|eval| & \verb|str| & \verb|typecheck| \\
    Constant & 1 & 1 & 0 \\
    BinaryAdd & 1 & 1 & 0 \\
    UnaryNegate & 1 & 1 & 0
  \end{tabular}
  \caption{C++ expression matrix}
  \label{tab:cpp}
\end{table}
\begin{table}
  \centering
  \begin{tabular}{rcc}
    & \verb|eval| & \verb|str| \\
    Constant & 1 & 1 \\
    BinaryAdd & 1 & 1 \\
    UnaryNegate & 0 & 0
  \end{tabular}
  \caption{Haskell expression matrix}
  \label{tab:haskell}
\end{table}

\section*{A Solution In Haskell}
As programming languages evolve, they tend to gain expressive power. Many features have been introduced to solve the expression problem. Object-oriented languages can use object algebras (a combination of the visitor pattern and parametric polymorphism)~\cite{oliveira2012extensibility} to add functionality efficiently. Haskell takes a different approach. Haskell can partially solve the expression problem with typeclasses, a construct introduced by Dr. Philip Wadler in the late 1980s~\cite{wadler-1989}. Let us walk through a solution.
%

Tying the expression types of \calclang{} together is what caused the expression problem. The problem can be sidestepped by allowing datatypes to be independent and attaching behavior separately.
\begin{minted}{haskell}
data Constant = Constant Double
data BinaryAdd left right = BinaryAdd left right
\end{minted}

Now we create the \verb|Expr| typeclass with no constraints. Since it has no constraints, any dataype can be an instance of \verb|Expr| simply by declaring itself as an instance. That gives a unifying class for \calclang{}'s expression types.
\begin{minted}{haskell}
class Expr e
instance Expr Constant
instance (Expr left, Expr right) => Expr (BinaryAdd left right)
\end{minted}

Each behavior of the expression types can be implemented as separate typeclasses. We implement the evaluation rules for constants and addition.
\begin{minted}{haskell}
class (Expr e) => Eval e where
  eval :: e -> Double
instance Eval Constant where
  eval (Constant c) = c
instance (Eval left, Eval right) => Eval (BinaryAdd left right) where
  eval (BinaryAdd l r) = eval l + eval r
\end{minted}
Now we demonstrate the efficacy of typeclasses as a solution to the expression problem. Adding a column to the expression matrix is demonstrably easy. We implement the pretty-printing functionality from previous examples.
\begin{minted}{haskell}
class (Expr e) => Str e where
  str :: e -> String
instance Str Constant where
  str (Constant c) = show c
instance (Str left, Str right) => Str (BinaryAdd left right) where
  str (BinaryAdd l r) = str l ++ " + " ++ str r
\end{minted}

Thus additional behavior can be added in accordance with the requirements of the expression problem. Now we demonstrate the ease of adding new datatypes, thus giving the ability to augment the expression matrix both dimensions.
\begin{minted}{haskell}
data UnaryNegate exp = UnaryNegate exp
instance (Expr exp) => Expr (UnaryNegate exp)
instance (Eval exp) => Eval (UnaryNegate exp) where
  eval (UnaryNegate e) = - (eval e)
instance (Str exp) => Str (UnaryNegate exp) where
  str (UnaryNegate e) = "-(" ++ str e ++ ")"
\end{minted}

Clearly, adding new behavior and dataypes can be done without modifying existing code, yet we claimed typeclasses are only a partial solution to the expression problem. Namely, now that \verb|Expr| is a unifying typeclass rather than unifying type, it is no longer possible for variables to have type \verb|Expr|. This precludes code such as:
\begin{minted}{haskell}
main = do
  let x = 4
  let y = if x > 3 then Constant 2.2 else BinaryAdd (Constant 1.0) (Constant 2.1)
  print y
\end{minted}

This problem can be and has been solved but the code gains significant complexity~\cite{swierstra2008data}. That said, the simple typeclass solution is still a great improvement. The ability to (nearly) solve the expression problem with simple, straightfoward patterns is a sign of a powerful and expressive language.

\section*{Trivializing the Expression Problem With Clojure}
Clojure is a functional programming language in the Lisp family. Rich Hickey, the original mind behind Clojure, was considering ways to solve the expression problem very early on in Clojure's lifecycle. Protocols are a feature introduced in version 1.2 of the language and are explicitly mentioned to be a solution to the expression problem.
\begin{quote}
  Avoid the 'expression problem' by allowing independent extension of the set of types, protocols, and implementations of protocols on types, by different parties~\cite{hickeyprotocols}.
\end{quote}

Protocols succeed in their purpose of trivializing the expression problem. We will walk through a solution to the expression problem in Clojure using protocols. Note the previous functions \verb|eval| and \verb|str| have been replaced by \verb|exp-eval| and \verb|to-str| to avoid naming collisions in Clojure's default namespace.

We begin by defining expression types and the desired behavior.
\begin{minted}{clojure}
(defrecord Constant [value])
(defrecord BinaryAdd [lhs rhs])

(defprotocol Evaluatable
  (exp-eval [this]))

(defprotocol Stringable
  (to-str [this]))
\end{minted}

Now we use the \verb|extend-type| macro to implement \verb|exp-eval| behavior for expressions.
\begin{minted}{clojure}
(extend-type Constant
  Evaluatable
    (exp-eval [this] (:value this)))
(extend-type BinaryAdd
  Evaluatable
    (exp-eval [this] (+ (exp-eval (:lhs this)) (exp-eval (:rhs this)))))
\end{minted}

Extending types with additional behavior is identical to adding behavior in the first place. The same is true for adding new types.
\begin{minted}{clojure}
(extend-type Constant
  Stringable
    (to-str [this] (str (:value this))))
(extend-type BinaryAdd
  Stringable
    (to-str [this] (str (to-str (:lhs this)) " + " (to-str (:rhs this)))))

(defrecord UnaryNegate [exp])
(extend-type UnaryNegate
  Evaluatable
    (exp-eval [this] (* -1 (exp-eval (:exp this))))
  Stringable
    (to-str [this] (str "-(" (to-str (:exp this)) ")")))
\end{minted}

Clearly, this solves the expression problem (at least) to the same degree as the presented Haskell solution. Clojure trivializes augmentation of the expression matrix in two directions.


\section*{Conclusion}
It is inarguable that some languages solve the expression more thoroughly than others. We are of the opinion that Clojure more thoroughly solves the problem than the other languages presented because of the intuitiveness of the solution. While Haskell requires specific programming patterns such as separation of datatypes and careful consideration of the composition of typeclasses, Clojure only requires a simple three-step process:
\begin{enumerate}
  \item Define the types.
  \item Define the behaviors.
  \item Implement the behaviors for the types.
\end{enumerate}
No programming patterns are required, nor must there be any concerns about language features interact. The solution is simple and straightforward. Since the expression problem is an indicator of the expressiveness of languages and expressiveness is not a formally computable concept, we feel this informal argument is sufficient to conclude that Clojure solves the expression problem in a ``better'' manner than Haskell or C++.

In any case, the expression problem remains a useful measure for the expressiveness of a language. In programming's pursuit of abstraction and more powerful constructs, more solutions to the expression problem may arise, each with different characteristics. Regardless of implementation or strategy, the expression problem remains a useful measure of programming languages.

\newpage
\printbibliography{}
\end{document}

%%% Local Variables:
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
