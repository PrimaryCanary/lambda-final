(defrecord Constant [value])
(defrecord BinaryAdd [lhs rhs])
(defrecord UnaryNegate [exp])

(defprotocol Evaluatable
  (exp-eval [this]))

(defprotocol Stringable
  (to-str [this]))

(extend-type Constant
  Evaluatable
    (exp-eval [this] (:value this))
  Stringable
    (to-str [this] (str (:value this))))

(extend-type BinaryAdd
  Evaluatable
    (exp-eval [this] (+ (exp-eval (:lhs this)) (exp-eval (:rhs this))))
  Stringable
    (to-str [this] (str (to-str (:lhs this)) " + " (to-str (:rhs this)))))

(extend-type UnaryNegate
  Evaluatable
    (exp-eval [this] (* -1 (exp-eval (:exp this))))
  Stringable
    (to-str [this] (str "-(" (to-str (:exp this)) ")")))

;;(let [exp (->BinaryAdd (->Constant 5.5) (->Constant 2.3))]
(let [exp (->UnaryNegate (->BinaryAdd
                      (->Constant 5.5)
                      (->BinaryAdd (->Constant 2.3) (->Constant -6.7))))]
  (println (to-str exp))
  (println (exp-eval exp)))
